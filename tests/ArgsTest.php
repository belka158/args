<?php
use PHPUnit\Framework\TestCase;
use Belka\Args;

class ArgsTest extends TestCase
{
    /**
     * @dataProvider provideDataForTestIsValid
     */
    public function testIsValid($schema, $args, $is_valid)
    {
        $args_valid = Args::create($schema, $args)->isValid();
        $this->assertEquals($is_valid, $args_valid);
    }

    public function provideDataForTestIsValid()
    {
        return [
            ["h", ["-ph"], false],
            ["h,p", ["-ph"], true],
            ["h,p", ["-p", "-h"], true],
            ["", ["-p"], false],
            ["p", [], true],
            ["", [], true],
            ["s*", [], true],
        ];
    }

    public function testGetBoolean()
    {
        $args = Args::create("h,p", ["-h"]);
        $this->assertTrue($args->getBoolean("h"));
        $this->assertFalse($args->getBoolean("p"));

    }

    public function testGetString()
    {
        $args = Args::create("s*,h", ["-s", "String", "-h"]);
        $this->assertEquals("String", $args->getString("s"));
    }

    public function testGetMixedArguments()
    {
        $args = Args::create("s*,h,p*", ["-s", "String", "-h"]);
        $this->assertEquals("String", $args->getString("s"));
        $this->assertEquals("", $args->getString("p"));
        $this->assertTrue($args->getBoolean("h"));
    }

    public function testHas()
    {
        $args = Args::create("s*,h", ["-h"]);
        $this->assertFalse($args->has("s"));
        $this->assertTrue($args->has("h"));
    }
}
