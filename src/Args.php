<?php
declare(strict_types=1);

namespace Belka;

use Ds\Map as Map;
use Ds\Set as Set;

class Args
{
    const ERROR_CODE_OK = 0;
    const ERROR_CODE_MISSING_STRING = 1;
    private $schema;
    private $args;
    private $valid = true;
    private $booleanArguments;
    private $stringArguments;
    private $argsFound;
    private $unexpectedArguments;
    private $errorCode = Args::ERROR_CODE_OK;
    private $currentArgument;
    private $errorArgument;

    public function __construct(string $schema, array $args)
    {
        $this->args = $args;
        $this->schema = $schema;
        $this->booleanArguments = new Map();
        $this->stringArguments = new Map();
        $this->argsFound = new Set();
        $this->unexpectedArguments = new Set();
        $this->valid = $this->parse();
    }

    public function isValid(): bool
    {
        return $this->valid;
    }

    private function parse(): bool
    {
        if (strlen($this->schema) == 0 && sizeof($this->args) == 0) {
            return true;
        }
        $this->parseSchema();
        $this->parseArguments();
        return $this->valid;
    }

    private function parseSchema()
    {
        foreach (explode(",", $this->schema) as $element) {
            if (strlen($element) > 0) {
                $trimmedElement = trim($element);
                $this->parseSchemaElement($trimmedElement);
            }
        }
    }

    private function parseSchemaElement(string $element)
    {
        $elementId = $element{0};
        $elementTail = substr($element, 1);
        $this->validateSchemaElementId($elementId);
        if ($this->isBooleanSchemaElement($elementTail)) {
            $this->parseBooleanSchemaElement($elementId);
        } elseif ($this->isStringSchemaElement($elementTail)){
            $this->parseStringSchemaElement($elementId);
        }
    }

    private function validateSchemaElementId(string $elementId)
    {
        if (!ctype_alpha($elementId)) {
            throw new \Exception("Bad character: {$elementId} in Args format: {$this->schema}");
        }
    }
    private function isBooleanSchemaElement($elementTail)
    {
        return strlen($elementTail) == 0;
    }

    private function isStringSchemaElement($elementTail)
    {
        return $elementTail == "*";
    }

    private function parseBooleanSchemaElement(string $element)
    {
        $this->booleanArguments[$element] = false;
    }

    private function parseStringSchemaElement(string $element)
    {
        $this->stringArguments[$element] = "";
    }

    private function parseArguments()
    {
        for ($this->currentArgument = 0; $this->currentArgument < sizeof($this->args); $this->currentArgument++) {
            $this->parseArgument($this->args[$this->currentArgument]);
        }
    }

    private function parseArgument(string $arg)
    {
        if ($arg{0} == "-") {
            $this->parseElements($arg);
        }
    }

    private function parseElements(string $arg)
    {
        for ($i = 1; $i < strlen($arg); $i++) {
            $this->parseElement($arg{$i});
        }
    }

    private function parseElement(string $argChar)
    {
        if ($this->setArgument($argChar)) {
            $this->argsFound->add($argChar);
        } else {
            $this->unexpectedArguments->add($argChar);
            $this->valid = false;
        }
    }

    private function setArgument(string $argChar): bool
    {
        $set = true;
        if ($this->isBoolean($argChar)) {
            $this->setBooleanArgument($argChar, true);
        } elseif ($this->isString($argChar)) {
            $this->setStringArgument($argChar, "");
        } else {
            $set = false;
        }
        return $set;
    }

    private function setBooleanArgument(string $argChar, bool $value)
    {
        $this->booleanArguments[$argChar] = $value;
    }

    private function isBoolean(string $argChar)
    {
        return $this->booleanArguments->hasKey($argChar);
    }

    private function setStringArgument(string $argChar, string $value)
    {
        $this->currentArgument++;
        try {
            $this->stringArguments[$argChar] = $this->args[$this->currentArgument];
        } catch (\Exception $e) {
            $this->valid = false;
            $this->errorArgument = $argChar;
            $this->errorCode = self::ERROR_CODE_MISSING_STRING;
        }
    }

    private function isString(string $argChar)
    {
        return $this->stringArguments->hasKey($argChar);
    }

    public function getBoolean(string $argChar): bool
    {
        return self::falseIfNull($this->booleanArguments[$argChar]);
    }

    public function getString(string $argChar): string
    {
        return self::blankIfNull($this->stringArguments[$argChar]);
    }

    private static function blankIfNull(string $value): string
    {
        return is_null($value) ? "" : $value;
    }

    private static function falseIfNull(bool $value): bool
    {
        return is_null($value) ? false : $value;
    }

    public function cardinality(): int
    {
        return $this->argsFound->count();
    }

    public function usage(): string
    {
        if (strlen($this->schema) != 0) {
            return "-[{$this->schema}]";
        } else {
            return "";
        }
    }

    public function errorMessage(): string
    {
        if ($this->unexpectedArguments->count() > 0) {
            return $this->unexpectedArgumentMessage();
        } else {
            switch ($this->errorCode) {
                case self::ERROR_CODE_MISSING_STRING:
                    return "Could not find string parameter for -{$this->errorArgument}";
                case self::ERROR_CODE_OK:
                    throw new \Exception("TILT: Should not get here");
            }
            return "";
        }
    }

    private function unexpectedArgumentMessage(): string
    {
        $message = "Argument(s) - ";
        foreach ($this->unexpectedArguments as $char) {
            $message .= $char;
        }
        $message .= " unexpected.";
        return $message;
    }

    public static function create($schema, $args): Args
    {
        return new self($schema, $args);
    }

    public function has(string $argChar)
    {
        return $this->argsFound->contains($argChar);
    }
}
